<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Theme;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/themes_cached/", name="themes_cached")
     */
    public function themeCachedAction(Request $request)
    {
        $arThemes = $this->getDoctrine()
            ->getRepository(Theme::class)
            ->createQueryBuilder('c')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        foreach($arThemes as &$arTheme){
            $arTheme["name"] = mb_convert_encoding($arTheme["name"], 'utf-8', mb_detect_encoding($arTheme["name"]));
        }

        $headers = array( 'Content-type' => 'application/json; charset=utf-8' );
        $response = new \Symfony\Component\HttpFoundation\Response( json_encode(array("themes" => $arThemes), JSON_UNESCAPED_UNICODE), 200, $headers );
        return $response;

        /*
        $response = new JsonResponse(array("themes" => $arThemes));
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        return $response;
        */
    }

    /**
     * @Route("/themes/", name="themes")
     */
    public function themeAction(Request $request)
    {
        $apikey = $this->container->getParameter('youscankey');
        $headers = array( 'Content-type' => 'application/json; charset=utf-8' );
        $response = new \Symfony\Component\HttpFoundation\Response( file_get_contents("https://api.youscan.io/api/external/themes?apikey=" . $apikey), 200, $headers );
        return $response;

        /*
        $response = new JsonResponse(array("themes" => $arThemes));
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        return $response;
        */
    }
}
