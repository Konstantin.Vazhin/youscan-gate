<?php
/**
 * Created by PhpStorm.
 * User: kaeexleen
 * Date: 28.10.17
 * Time: 23:13
 */
// src/AppBundle/Command/SaveYouscanData.php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Theme;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SaveYouscanData extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:save-youscan')

            // the short description shown while running "php bin/console list"
            ->setDescription('Saves youscan data to table.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to save youscan data to table...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apikey = $this->getContainer()->getParameter('youscankey');
        $arThemes = json_decode(file_get_contents("https://api.youscan.io/api/external/themes?apikey=" . $apikey), true);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        if (!empty($arThemes["themes"])) {
            $query = $em->createQuery('DELETE AppBundle:Theme');
            $query->execute();

            foreach ($arThemes["themes"] as $arTheme) {
                $theme = new Theme();
                $theme->setName($arTheme["name"]);
                $theme->setId($arTheme["id"]);

                $em->persist($theme);

                $em->flush();
            }

            $output->writeln('Youscan Data Saved');
        }
    }
}